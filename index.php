<?php
require 'db.php';
require 'functions.php';
require 'vendor/autoload.php';

$app = new \Slim\Slim();

//Routes
$app->get('/hello/:name','getHello');
                   
/*****
 * standard api routes 
 * ***/
/* 1 */  $app->get('/users','getUsers');                                                         //Retreives lists of users
/* 2 */  $app->get('/users/:user_id','getUserByUserid');                                         //Retreives particular user info
/* 3 */  $app->post('/users','postUsers');                                                       //Creates new user
/* 4 */  $app->put('/users/:user_id','putUsers');                                                //Updates particular user
/* 5 */  $app->put('/users/password/:user_id','putUsersPassword');
/* 6 */  $app->delete('/users/:user_id','deleteUsers');                                          //deletes a particular user
/* 7 */  $app->get('/users/:user_id/interests','getUserInterestsByUserid');                      //Get the particular user's interest list
/* 8 */  $app->post('/users/:user_id/interests','postUserInterestsByUserid');                    //Insert new interests for user
/* 9 */  $app->get('/interests','getInterests');                                                 //get all interests
/* 10 */ $app->get('/interests/:intid','getInterestByIntid');                                    //Retreive particular interest by intid
/* 11 */ $app->get('/users/:user_id/accept_reject_list','getUserAcceptRejectListByUserid');      //Get particulat users accept reject list
/* 12 */ $app->post('/users/:user_id/accept_reject_list','postUserAcceptRejectListByUserid');    //Insert new entity in users accept reject list
/* 13 */ $app->get('/accept_reject_list/:arid','getAcceptRejectListByArid');                     //Get details about particular arList entity
/* 14 */ $app->delete('/accept_reject_list/:arid','deleteAcceptRejectListByArid');               //delete a particular ar_list entity using arid
/* 15 */ $app->delete('/interests/:user_intid','deleteInterestsByUserintid');                    //deletes a particular user interest
/* 16 */ $app->post('/users/cv/:user_id','postUserCvByUserid');
/* 17 */ $app->get('/file/:file_name','getFile');

//Non-noun(Verb) Api routes
/* 18 */ $app->post('/login', 'postLogin');
/* 19 */ $app->get('/checkLogin/:code','checkLogin');
/* 20 */ $app->get('/getUserInfo','getUserInfo');

//Api related information
/* 21 */  $app->get('/users/ussage/:user_id','getUsersUssage');

/*****
 * standard api routes methods 
 * ***/
//Retreives lists of users
function getUsers()
{
     //Retreives lists of users
     $app = \Slim\Slim::getInstance();
      $req = $app->request();
       $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
      $headers = getallheaders();
      $orderby_array = array('user_id','email','first_name','last_name','added_on','last_login');
        $keyword='';
        $orderby='user_id';
        $order='desc';
        $limit=6;
        $offset=0;
        if($app->request()->get('keyword')!=null)
        {
            $keyword=$app->request()->get('keyword');
        }
        if($app->request()->get('orderby')!=null && in_array($app->request()->get('orderby'), $orderby_array) )
        {
            $orderby=$app->request()->get('orderby');
        }
        if($app->request()->get('order')!=null && ( $app->request()->get('order')=='asc' || $app->request()->get('order')=='desc' ) )
        {
            $order=$app->request()->get('order');
        }
        if($app->request()->get('limit')!=null)
        {
            $limit= (int) $app->request()->get('limit');
        }
        if($app->request()->get('offset')!=null)
        {
            $offset= (int) $app->request()->get('offset');
        }
        $user = array();
        if(!isset($headers['Authorization']) || $headers['Authorization']=='' )
        {
            $app->response->setStatus(403);
            
        }
        else{
      $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state,api_key,cv_file FROM users where api_key = '".$headers['Authorization']."'";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
     
      $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200); 
      if($user){ 
          $dt = array('user_id'=>$user->user_id,'action_type'=>'search');
          trackUserActivity($dt);
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
          $where =' where 1=1 ';
          $orderstr = ' order by '.$orderby.' '.$order.' ';
          if($keyword!=''){
          $where .= " and ( email like '%".$keyword."%' or first_name like '%".$keyword."%' or last_name like '%".$keyword."%'  ) ";
          }
          $users = array();
          $total = 0;
          $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state from users ".$where."  ".$orderstr."  Limit $offset,$limit  ";
          //$response['query'] =  $sql;
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
            $sql = "SELECT email,first_name,last_name,last_login from users ".$where."   ";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $total = count($stmt->fetchAll(PDO::FETCH_OBJ));
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
          $response['response']['results'] = $users;
          $response['response']['total'] = $total;
          $response['response']['offset'] = $offset;
          $response['response']['limit'] = $limit;
      }
        }
    echo json_encode($response);
}
//Retreives particular user info
function getUserByUserid($user_id)
{
     //Retreives particular user info
    
      $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
       $headers = getallheaders();
      $user = array();
      if(!isset($headers['Authorization']) || $headers['Authorization']=='' )
        {
            $app->response->setStatus(403);
            
        }
        else{
      $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state,api_key,cv_file FROM users where api_key = '".$headers['Authorization']."'";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          if($user){
              $dt = array('user_id'=>$user->user_id,'action_type'=>'view_profile');
          trackUserActivity($dt);
           $sql = "SELECT email,first_name,last_name,last_login,address,gender,city,postcode,country,state FROM users where user_id = ".(int)$user_id."";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            $response['response'] = $users;
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
          
        }
        }
    echo json_encode($response);

}
//inserts new user
function postUsers()
{
     //Inserts new user
    
    $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
    //signup user
    $req = $app->request();
    //Validate all posted requests
    $first_name = urldecode($req->post('first_name'));
    $last_name = urldecode($req->post('last_name'));
    $email = urldecode($req->post('email'));
    $password = urldecode($req->post('password'));
    if($first_name=='' || $last_name=='' || $email=='' || $password=='' )
    {//check if empty
        $response['status'] = 'fail';
        $response['message'] = 'All feilds are mandatory. Please fill all the feilds.';
        
    }
    else 
    {
        if(!isValidEmail($email))
        {//email valid or not
            $response['status'] = 'fail';
            $response['message'] = 'Please enter a valid email address';
        }
        else 
        {
            $emailExists = isEmailExists($email);
            if(isset($emailExists->email) )
            {
                $response['status'] = 'fail';
                $response['message'] = 'This email is already registered.';
            }
            else
            {
                //insert user in db
                $sql = "INSERT INTO users (first_name, last_name, email, password,  added_on , api_key)
                        VALUES ('".$first_name."', '".$last_name."', '".$email."', '".md5($password)."', ".time()." , '".md5(uniqid())."' )";
                try {
                $db = getDB();
                $stmt = $db->query($sql);
                $db = null;
                $app->response->setStatus(201);
                $response['status'] = 'success';
                $response['message'] = 'Signup successfull.';
                }catch(PDOException $e) {
                    $response['status'] = 'fail';
                    $response['message'] = 'There was an error during signup';
                }
                
            }
        }
    }
    echo json_encode($response);
}
//Updates particular user
function putUsers($user_id)
{
     //Updates particular user
    
    $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
   // $response['posted'] = json_encode($_POST);
    $app = \Slim\Slim::getInstance();
    $req = $app->request();
    $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(202);
      $headers = getallheaders();
    //signup user
    $user = array();
      if(!isset($headers['Authorization']) || $headers['Authorization']=='' )
        {
            $app->response->setStatus(403);
            
        }
        else{
            $old_password = trim(urldecode($req->post('old_password')));
                $new_password = trim(urldecode($req->post('new_password')));
                $c_password = trim(urldecode($req->post('c_password')));
                $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state,api_key FROM users where api_key = '".$headers['Authorization']."' and user_id = ".$user_id." ";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
     
      $app->response->headers->set('Content-Type', 'application/json');
      //$app->response->setStatus(200); 
      if($user){
          $dt = array('user_id'=>$user->user_id,'action_type'=>'edit_profile');
          trackUserActivity($dt);
    //Validate all posted requests
    $address = trim(urldecode($req->post('address')));
    $city = trim(urldecode($req->post('city')));
    $country = trim(urldecode($req->post('country')));
    $first_name = trim(urldecode($req->post('first_name')));
    $gender = trim(urldecode($req->post('gender')));
    $last_name = trim(urldecode($req->post('last_name')));
    $postcode = trim(urldecode($req->post('postcode')));
    $state = trim(urldecode($req->post('state')));
    $password = trim(urldecode($req->post('password')));
                $dat = array(
                                            'user_id' => $user_id,
                                            'address' =>$address,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'city' => $city,
                                            'country' => $country,
                                            'gender' => $gender,
                                            'postcode' => $postcode,
                                            'password' => $password,
                                            'state' => $state
                                            );
               // $app->setCookie('auth', $emailExists->email, '2 days');
                if(updateUser($dat) =='true' )
                {
                $response['status'] = 'success';
                $response['message'] = 'successfully edited profile.';
                }
   //  echo json_encode(array($req->post('first_name'),$req->post('last_name'),$req->post('email'),$req->post('password')));
      }
     }
    echo json_encode($response);
}
//Updates particular user
function putUsersPassword($user_id)
{
     //Updates particular user
    
    $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
   // $response['posted'] = json_encode($_POST);
    $app = \Slim\Slim::getInstance();
    $req = $app->request();
    $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(202);
      $headers = getallheaders();
      $user = array();
      if(!isset($headers['Authorization']) || $headers['Authorization']=='' )
        {
            $app->response->setStatus(403);
            
        }
        else{
            $old_password = trim(urldecode($req->post('old_password')));
                $new_password = trim(urldecode($req->post('new_password')));
                $c_password = trim(urldecode($req->post('c_password')));
                $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state,api_key FROM users where api_key = '".$headers['Authorization']."' and password = '".md5($old_password)."' ";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
     
      $app->response->headers->set('Content-Type', 'application/json');
      //$app->response->setStatus(200); 
      if($user){
                $dt = array('user_id'=>$user->user_id,'action_type'=>'edit_profile');
                trackUserActivity($dt);
                //Validate all posted requests
                
                            $dat = array(
                                                        'user_id' => $user_id,
                                                        'password' => $new_password,
                                                        );
                           // $app->setCookie('auth', $emailExists->email, '2 days');
                            if(updateUser($dat) =='true' )
                            {
                            $response['status'] = 'success';
                            $response['message'] = 'successfully edited profile.';
                            }
               //  echo json_encode(array($req->post('first_name'),$req->post('last_name'),$req->post('email'),$req->post('password')));
        }
        else
        {
            $response['status'] = 'fail';
                            $response['message'] = 'Cannot update password.';
        }
        }
        echo json_encode($response);
}

//deletes a particular user
function deleteUsers($user_id)
{
     //deletes a particular user
    $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
          
           $sql = "DELETE FROM users WHERE user_id = ".(int)$user_id."";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
          //  $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            $response['response'] = 'Deleted';
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
    echo json_encode($response);
}
//Get the particular user's interest list
function getUserInterestsByUserid($user_id)
{
     //Get the particular user's interest list
    $app = \Slim\Slim::getInstance();
     $orderby_array = array('user_id','intid','intname','added_on');
        $keyword='';
        $orderby='user_intid';
        $order='desc';
        $limit=6;
        $offset=0;
        $all = false;
        if($app->request()->get('keyword')!=null)
        {
            $keyword=$app->request()->get('keyword');
        }
        if($app->request()->get('orderby')!=null && in_array($app->request()->get('orderby'), $orderby_array) )
        {
            $orderby=$app->request()->get('orderby');
        }
        if($app->request()->get('order')!=null && ( $app->request()->get('order')=='asc' || $app->request()->get('order')=='desc' ) )
        {
            $order=$app->request()->get('order');
        }
        if($app->request()->get('limit')!=null)
        {
            $limit= (int) $app->request()->get('limit');
        }
        if($app->request()->get('offset')!=null)
        {
            $offset= (int) $app->request()->get('offset');
        }
        if($app->request()->get('all')!=null)
        {
            $all = true;
        }
      
        $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
          
           $response['status'] = 'success';
          $response['message'] = 'Authenticated';
          $where =' where ui.user_id= '.$user_id.' ';
          $orderstr = ' order by '.$orderby.' '.$order.' ';
          if($keyword!=''){
          $where .= " and ( intname like '%".$keyword."%'  ) ";
          }
          $users = array();
          $total = 0;
          if($all)
          {
              $sql = "select ui.user_intid,ui.user_id,ui.added_on,i.intid,i.intname,i.intdesc from user_interests ui join interests i on ui.intid=i.intid ".$where."  ".$orderstr."   ";
          }
          else
          {
              $sql = "select ui.user_intid,ui.user_id,ui.added_on,i.intid,i.intname,i.intdesc from user_interests ui join interests i on ui.intid=i.intid ".$where."  ".$orderstr."  Limit $offset,$limit  ";
          }
          //$response['query'] =  $sql;
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
            $sql = "select ui.user_intid,ui.user_id,ui.added_on,i.intid,i.intname,i.intdesc from user_interests ui join interests i on ui.intid=i.intid ".$where."   ";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $total = count($stmt->fetchAll(PDO::FETCH_OBJ));
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
          $response['response']['results'] = $users;
          $response['response']['total'] = $total;
          $response['response']['offset'] = $offset;
          $response['response']['limit'] = $limit;
    echo json_encode($response);
    //session_destroy(); 

}
//Insert new interests for user
function postUserInterestsByUserid($user_id)
{
     //Insert new interests for user
     $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
    //signup user
    $req = $app->request();
    //Validate all posted requests
    $userInterests = $req->post('userInterests');
    if(empty($userInterests))
    {//check if empty
        $response['status'] = 'fail';
        $response['message'] = 'All feilds are mandatory. Please fill all the feilds.';
        
    }
    else 
    {
                //delete existing user interests
                $sql = "DELETE FROM user_interests WHERE user_id = ".(int)$user_id."";
                try {
                $db = getDB();
                $stmt = $db->query($sql);
              //  $users = $stmt->fetch(PDO::FETCH_OBJ);
                $db = null;
               // $response['response'] = 'Deleted';

                } catch(PDOException $e) {
                   // $response['response'] = 'false';
                }
                
                foreach($userInterests as $intr ){
                //insert user in db
                $sql = "INSERT INTO user_interests (intid, user_id,  added_on)
                        VALUES ( ".$intr.", ".$user_id.", ".time()." )";
                try {
                $db = getDB();
                $stmt = $db->query($sql);
                $db = null;
                $app->response->setStatus(201);
               $response['status'] = 'success';
                $response['message'] = 'Insertion successfull.';
                }catch(PDOException $e) {
                    $response['status'] = 'fail';
                    $response['message'] = 'There was an error during signup';
                }
                }
            
        
    }
    echo json_encode($response);
}
//get all interests
function getInterests()
{
     //get all interests
    $all = false;
     $app = \Slim\Slim::getInstance();
      $req = $app->request();
      $orderby_array = array('intid','intname');
        $keyword='';
        $orderby='intid';
        $order='desc';
        $limit=6;
        $offset=0;
        if($app->request()->get('keyword')!=null)
        {
            $keyword=$app->request()->get('keyword');
        }
        if($app->request()->get('orderby')!=null && in_array($app->request()->get('orderby'), $orderby_array) )
        {
            $orderby=$app->request()->get('orderby');
        }
        if($app->request()->get('order')!=null && ( $app->request()->get('order')=='asc' || $app->request()->get('order')=='desc' ) )
        {
            $order=$app->request()->get('order');
        }
        if($app->request()->get('limit')!=null)
        {
            $limit= (int) $app->request()->get('limit');
        }
        if($app->request()->get('offset')!=null)
        {
            $offset= (int) $app->request()->get('offset');
        }
        if($app->request()->get('all')!=null)
        {
            $all = true;
        }
    $code='fgh';
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
      $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200); 
      if($code !=''  ){    
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
          $where =' where 1=1 ';
          $orderstr = ' order by '.$orderby.' '.$order.' ';
          if($keyword!=''){
          $where .= " and ( intname like '%".$keyword."%'  ) ";
          }
          $users = array();
          $total = 0;
          if($all)
          {
                $sql = "select * from interests ".$where."  ".$orderstr."    ";
          }
          else
          {
                $sql = "select * from interests ".$where."  ".$orderstr."  Limit $offset,$limit  ";
          }
          //$response['query'] =  $sql;
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
            $sql = "select * from interests ".$where."   ";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $total = count($stmt->fetchAll(PDO::FETCH_OBJ));
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
          $response['response']['results'] = $users;
          $response['response']['total'] = $total;
          $response['response']['offset'] = $offset;
          $response['response']['limit'] = $limit;
      }
    echo json_encode($response);
}
//Retreive particular interest by intid
function getInterestByIntid($intid)
{
     //Retreive particular interest by intid
     $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
          
           $sql = "SELECT * FROM interests where intid = ".(int)$intid."";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            $response['response'] = $users;
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
    echo json_encode($response);
}
//Get particulat users accept reject list
function getUserAcceptRejectListByUserid($user_id)
{
     //Get particulat users accept reject list
    $app = \Slim\Slim::getInstance();
     $orderby_array = array('ref_uid','first_name','last_name','email','added_on');
        $keyword='';
        $orderby='arid';
        $order='desc';
        $limit=6;
        $offset=0;
        if($app->request()->get('keyword')!=null)
        {
            $keyword=$app->request()->get('keyword');
        }
        if($app->request()->get('orderby')!=null && in_array($app->request()->get('orderby'), $orderby_array) )
        {
            $orderby=$app->request()->get('orderby');
        }
        if($app->request()->get('order')!=null && ( $app->request()->get('order')=='asc' || $app->request()->get('order')=='desc' ) )
        {
            $order=$app->request()->get('order');
        }
        if($app->request()->get('limit')!=null)
        {
            $limit= (int) $app->request()->get('limit');
        }
        if($app->request()->get('offset')!=null)
        {
            $offset= (int) $app->request()->get('offset');
        }
    
      
        $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
          
           $response['status'] = 'success';
          $response['message'] = 'Authenticated';
          $where =' where ar.uid= '.$user_id.' ';
          $orderstr = ' order by '.$orderby.' '.$order.' ';
          if($keyword!=''){
          $where .= " and ( first_name like '%".$keyword."%' or last_name like '%".$keyword."%' or email like '%".$keyword."%'  ) ";
          }
          $users = array();
          $total = 0;
          $sql = "select ar.*,u.email,u.first_name,u.last_name,u.gender,u.address,u.city,u.country,u.state,u.postcode from acccept_reject ar
join users u on ar.ref_uid = u.user_id ".$where."  ".$orderstr."  Limit $offset,$limit  ";
         // $response['query'] =  $sql;
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
            $sql = "select ar.*,u.email,u.first_name,u.last_name,u.gender,u.address,u.city,u.country,u.state,u.postcode from acccept_reject ar
join users u on ar.ref_uid = u.user_id ".$where."   ";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $total = count($stmt->fetchAll(PDO::FETCH_OBJ));
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
          $response['response']['results'] = $users;
          $response['response']['total'] = $total;
          $response['response']['offset'] = $offset;
          $response['response']['limit'] = $limit;
    echo json_encode($response);
}
//Insert new entity in users accept reject list
function postUserAcceptRejectListByUserid($user_id)
{
     //Insert new entity in users accept reject list
}
//Get details about particular arList entity
function getAcceptRejectListByArid($arid)
{
     //Get details about particular arList entity
    $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
          
           $sql = "SELECT * FROM acccept_reject where arid = ".(int)$arid."";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            $response['response'] = $users;
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
    echo json_encode($response);
}
//deletes a particular arList entity
function deleteAcceptRejectListByArid($arid)
{
     //deletes a particular arList entity
     $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
          
           $sql = "DELETE FROM acccept_reject WHERE arid = ".(int)$arid."";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
          //  $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            $response['response'] = 'Deleted';
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
    echo json_encode($response);
}
//deletes a particular user interest
function deleteInterestsByUserintid($user_initid)
{
     //deletes a particular user interest
    $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
          
           $sql = "DELETE FROM user_interests WHERE user_initid = ".(int)$user_initid."";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
          //  $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            $response['response'] = 'Deleted';
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
    echo json_encode($response);
}


//Route Functions
 function getHello($name) {
      $app = \Slim\Slim::getInstance();
    $app->response->setStatus(400);  
    echo json_encode('hello');
};
$app->get('/groups/test/', function() use ($app) {
    $test = $app->request()->get('fields');
    echo "This is a GET route with $test";
});


function checkLogin($code) {
      $app = \Slim\Slim::getInstance();
    //echo "Hello, $name";
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
      $users = array();
      $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state,api_key FROM users where api_key = '".$code."'";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
      //$response['response']['users'] = $users;
      if($users  ){
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
          $headers = $app->request->headers->get('Authorization');
          $response['response'] = getallheaders();

      }
    echo json_encode($response);
    //session_destroy(); 
};

function getUserInfo() {
      $app = \Slim\Slim::getInstance();
      $app->response->setStatus(200);
      $headers = getallheaders();
    //echo "Hello, $name";
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
      $users = array();
      $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state,api_key,cv_file FROM users where api_key = '".$headers['Authorization']."'";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
      
      if($users  ){
          
          $response['response'] = $users;
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
      }
    echo json_encode($response);
    //session_destroy(); 
};



function postLogin()  {
    $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
   // $response['posted'] = json_encode($_POST);
    $app = \Slim\Slim::getInstance();
    //signup user
    $req = $app->request();
   // $email = 'upendramanve@gmail.com';
    //Validate all posted requests
    $email = urldecode($req->post('email'));
    $password = urldecode($req->post('password'));
    if( $email=='' || $password=='')
    {//check if empty
        $response['status'] = 'fail';
        $response['message'] = 'All feilds are mandatory. Please fill all the feilds.';
        
    }
    else 
    {
        if(!isValidEmail($email))
        {//email valid or not
            $response['status'] = 'fail';
            $response['message'] = 'Please enter a valid email address';
        }
        else 
        {
            $emailExists = isEmailPasswordExists($email,$password);
            if(isset($emailExists->email) )
            {
               // $app->setCookie('auth', $emailExists->email, '2 days');
                updateLastLogin($email);
                $dt = array('user_id'=>$emailExists->user_id,'action_type'=>'login');
          trackUserActivity($dt);
                $response['status'] = 'success';
                $response['message'] = 'Login successfull.';
                $response['response'] = array('user_token'=>$emailExists->api_key);
            }
            else
            {
               $response['status'] = 'fail';
                $response['message'] = 'Invalid username and/or password. ';
                
            }
        }
    }
   //  echo json_encode(array($req->post('first_name'),$req->post('last_name'),$req->post('email'),$req->post('password')));
    echo json_encode($response);
};

function getUsersUssage($user_id)
{
     //Retreives particular user info
    
      $app = \Slim\Slim::getInstance();
      $statusCode = 200;
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
       $headers = getallheaders();
      $user = array();
      if(!isset($headers['Authorization']) || $headers['Authorization']=='' )
        {
            //$app->response->setStatus(403);
            $statusCode = 403;
        }
        else{
      $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state,api_key,cv_file FROM users where api_key = '".$headers['Authorization']."' and user_id = ".(int)$user_id."  ";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          if($user){
           $sql = "SELECT* FROM track_user_activity where user_id = ".(int)$user_id."";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            $response['response']['results'] = $users;
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
          
        }
        }
     echoResponse($statusCode,$response);

}

function postUserCvByUserid($user_id)
{
    $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
      $app->response->setStatus(200);
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
       $headers = getallheaders();
      $user = array();
      if(!isset($headers['Authorization']) || $headers['Authorization']=='' )
        {
            $app->response->setStatus(403);
            
        }
        else{
      $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state,api_key FROM users where api_key = '".$headers['Authorization']."' and user_id = ".(int)$user_id."  ";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          if($user){
             //$response['response']['files'] = $_FILES;
              if(!empty($_FILES)){
                $response['response']['count'] = count($_FILES);
                 for($i=0; $i < $response['response']['count'] ; $i++  )
                 {
                    // ini_set('upload_max_filesize', '10M');
                   // ini_set('post_max_size', '10M');
                   // ini_set('max_input_time', 300);
                   // ini_set('max_execution_time', 300);
                    $info = pathinfo($_FILES['file-'.$i]['name']);
                    $ext = $info['extension']; // get the extension of the file
                   // list($oldname,$ext) = explode(".",$info);
                    $newname = uniqid('file_').'.'.$ext; 

                    $target = 'uploads/'.$newname;
                    move_uploaded_file( $_FILES['file-'.$i]['tmp_name'], $target);
                    while (!file_exists($target)) sleep(1);
                    updateUser(array('user_id'=>$user_id,'cv_file'=>$newname));
                 }
                 $response['status'] = 'success';
                 
                 $response['message'] = 'CV uploaded successfully.';
              }
          }
        }
    echo json_encode($response);
    
}

function getFile($filename)
{
    $file = 'uploads/'.$filename;
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
}


$app->run();