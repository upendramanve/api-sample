<?php
session_start();
require 'db.php';
require 'vendor/autoload.php';

$app = new \Slim\Slim();

//Routes
$app->get('/hello/:name','getHello');
$app->get('/checkLogin/:code','checkLogin');
$app->get('/getUserInfo/:code','getUserInfo');
$app->get('/getUser/:code/:userid','getUser');
//$app->get('/searchUser/:code(/:keyword(/:orderby(/:limit(/:offset))))','searchUser'); 
$app->get('/searchUser/:code','searchUser');
$app->get('/logout/:code','logout');
$app->post('/signup', 'postSignup');
$app->post('/login', 'postLogin');
$app->post('/editProfile/:code', 'postEditProfile');

//Route Functions
 function getHello($name) {
      $app = \Slim\Slim::getInstance();
    $app->response->setStatus(400);  
    echo json_encode('hello');
    //echo json_encode($_SESSION);
    //session_destroy(); 
};
$app->get('/groups/test/', function() use ($app) {
    $test = $app->request()->get('fields');
    echo "This is a GET route with $test";
});
//function searchUser($code,$keyword='',$orderby='user_id',$limit=5,$offset=0) {
function searchUser($code) {    
    $app = \Slim\Slim::getInstance();
      $req = $app->request();
      $orderby_array = array('user_id','email','first_name','last_name','added_on','last_login');
        $keyword='';
        $orderby='user_id';
        $order='desc';
        $limit=6;
        $offset=0;
        if($app->request()->get('keyword')!=null)
        {
            $keyword=$app->request()->get('keyword');
        }
        if($app->request()->get('orderby')!=null && in_array($app->request()->get('orderby'), $orderby_array) )
        {
            $orderby=$app->request()->get('orderby');
        }
        if($app->request()->get('order')!=null && ( $app->request()->get('order')=='asc' || $app->request()->get('order')=='desc' ) )
        {
            $order=$app->request()->get('order');
        }
        if($app->request()->get('limit')!=null)
        {
            $limit= (int) $app->request()->get('limit');
        }
        if($app->request()->get('offset')!=null)
        {
            $offset= (int) $app->request()->get('offset');
        }
    
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
    // if(isset($_SESSION['user']['unique']) && $_SESSION['user']['unique']==$code  ){
      if($code !=''  ){    
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
          $where =' where 1=1 ';
          $orderstr = ' order by '.$orderby.' '.$order.' ';
          if($keyword!=''){
          $where .= " and ( email like '%".$keyword."%' or first_name like '%".$keyword."%' or last_name like '%".$keyword."%'  ) ";
          }
          $users = array();
          $total = 0;
          $sql = "SELECT user_id,email,first_name,last_name,last_login,address,gender,city,postcode,country,state from users ".$where."  ".$orderstr."  Limit $offset,$limit  ";
          //$response['query'] =  $sql;
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
            $sql = "SELECT email,first_name,last_name,last_login from users ".$where."   ";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $total = count($stmt->fetchAll(PDO::FETCH_OBJ));
            $db = null;
            } catch(PDOException $e) {
                $response['response'] =$sql;
            }
          $response['response']['results'] = $users;
          $response['response']['total'] = $total;
          $response['response']['offset'] = $offset;
          $response['response']['limit'] = $limit;
      }
    echo json_encode($response);
   // echo "Code: $code --- Keyword: $keyword --- Orderby: $orderby --- Limit: $limit --- Offset --- $offset ";
    //echo json_encode($_SESSION);
   
};

function checkLogin($code) {
      $app = \Slim\Slim::getInstance();
    //echo "Hello, $name";
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
      if(isset($_SESSION['user']['unique']) && $_SESSION['user']['unique']==$code  ){
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
      }
    echo json_encode($response);
    //session_destroy(); 
};

function getUserInfo($code) {
      $app = \Slim\Slim::getInstance();
      $app->response->setStatus(200);
    //echo "Hello, $name";
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
      if(isset($_SESSION['user']['unique']) && $_SESSION['user']['unique']==$code  ){
          
           $sql = "SELECT email,first_name,last_name,last_login,address,gender,city,postcode,country,state FROM users where email = '".$_SESSION['user']['email']."'";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            $response['response'] = $users;
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
      }
    echo json_encode($response);
    //session_destroy(); 
};

function getUser($code,$userid) {
      $app = \Slim\Slim::getInstance();
    //echo "Hello, $name";
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
      if(isset($_SESSION['user']['unique']) && $_SESSION['user']['unique']==$code  ){
          
           $sql = "SELECT email,first_name,last_name,last_login,address,gender,city,postcode,country,state FROM users where user_id = ".(int)$userid."";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            $users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            $response['response'] = $users;
            
            } catch(PDOException $e) {
                $response['response'] = 'false';
            }
          
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
      }
    echo json_encode($response);
    //session_destroy(); 
};

function logout($code) {
      $app = \Slim\Slim::getInstance();
    //echo "Hello, $name";
      $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
      if(isset($_SESSION['user']['unique']) && $_SESSION['user']['unique']==$code  ){
          session_destroy();
          $response['status'] = 'success';
          $response['message'] = 'Authenticated';
      }
    echo json_encode($response);
    //session_destroy(); 
};

 function postSignup()  {
    $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
    $app = \Slim\Slim::getInstance();
    //signup user
    $req = $app->request();
    //Validate all posted requests
    $first_name = urldecode($req->post('first_name'));
    $last_name = urldecode($req->post('last_name'));
    $email = urldecode($req->post('email'));
    $password = urldecode($req->post('password'));
    if($first_name=='' || $last_name=='' || $email=='' || $password=='')
    {//check if empty
        $response['status'] = 'fail';
        $response['message'] = 'All feilds are mandatory. Please fill all the feilds.';
        
    }
    else 
    {
        if(!isValidEmail($email))
        {//email valid or not
            $response['status'] = 'fail';
            $response['message'] = 'Please enter a valid email address';
        }
        else 
        {
            $emailExists = isEmailExists($email);
            if(isset($emailExists->email) )
            {
                $response['status'] = 'fail';
                $response['message'] = 'This email is already registered.';
            }
            else
            {
                //insert user in db
                $sql = "INSERT INTO users (first_name, last_name, email, password,  added_on)
                        VALUES ('".$first_name."', '".$last_name."', '".$email."', '".md5($password)."', ".time()." )";
                try {
                $db = getDB();
                $stmt = $db->query($sql);
                $db = null;
                $response['status'] = 'success';
                $response['message'] = 'Signup successfull.';
                }catch(PDOException $e) {
                    $response['status'] = 'fail';
                    $response['message'] = 'There was an error during signup';
                }
                
            }
        }
    }
   //  echo json_encode(array($req->post('first_name'),$req->post('last_name'),$req->post('email'),$req->post('password')));
    echo json_encode($response);
};

function postLogin()  {
    $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
   // $response['posted'] = json_encode($_POST);
    $app = \Slim\Slim::getInstance();
    //signup user
    $req = $app->request();
   // $email = 'upendramanve@gmail.com';
    //Validate all posted requests
    $email = urldecode($req->post('email'));
    $password = urldecode($req->post('password'));
    if( $email=='' || $password=='')
    {//check if empty
        $response['status'] = 'fail';
        $response['message'] = 'All feilds are mandatory. Please fill all the feilds.';
        
    }
    else 
    {
        if(!isValidEmail($email))
        {//email valid or not
            $response['status'] = 'fail';
            $response['message'] = 'Please enter a valid email address';
        }
        else 
        {
            $emailExists = isEmailPasswordExists($email,$password);
            if(isset($emailExists->email) )
            {
                $unique = md5(uniqid());
                $dat = array(
                                            'user_id' => $emailExists->user_id,
                                            'first_name' => $emailExists->first_name,
                                            'last_name' => $emailExists->last_name,
                                            'email' => $emailExists->email,
                                            'unique' => $unique
                                            );
               // $app->setCookie('auth', $emailExists->email, '2 days');
                updateLastLogin($email);
                $_SESSION['user'] = $dat;
                $response['status'] = 'success';
                $response['message'] = 'Login successfull.';
                $response['response'] = array('user_token'=>$unique);
            }
            else
            {
               $response['status'] = 'fail';
                $response['message'] = 'Invalid username and/or password. ';
                
            }
        }
    }
   //  echo json_encode(array($req->post('first_name'),$req->post('last_name'),$req->post('email'),$req->post('password')));
    echo json_encode($response);
};

function postEditProfile($code)  {
    $response = array('status'=>'fail','message'=>'some error occured','response'=>'');
   // $response['posted'] = json_encode($_POST);
    $app = \Slim\Slim::getInstance();
    //signup user
    $req = $app->request();
    //Validate all posted requests
    $address = trim(urldecode($req->post('address')));
    $city = trim(urldecode($req->post('city')));
    $country = trim(urldecode($req->post('country')));
    $first_name = trim(urldecode($req->post('first_name')));
    $gender = trim(urldecode($req->post('gender')));
    $last_name = trim(urldecode($req->post('last_name')));
    $postcode = trim(urldecode($req->post('postcode')));
    $state = trim(urldecode($req->post('state')));
    
    if(isset($_SESSION['user']['unique']) && $_SESSION['user']['unique']==$code  ){
    if( $first_name=='' || $last_name=='' || $gender=='')
    {//check if empty
        $response['status'] = 'fail';
        $response['message'] = 'Please fill all the mandatory feilds.';
        
    }
    else 
    {
        
           
                $dat = array(
                                            'address' =>$address,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'city' => $city,
                                            'country' => $country,
                                            'gender' => $gender,
                                            'postcode' => $postcode,
                                            'state' => $state
                                            );
               // $app->setCookie('auth', $emailExists->email, '2 days');
                if(updateUser($dat) =='success' )
                {
                $response['status'] = 'success';
                $response['message'] = 'successfully edited profile.';
                }
        
    }
    }
   
   //  echo json_encode(array($req->post('first_name'),$req->post('last_name'),$req->post('email'),$req->post('password')));
    echo json_encode($response);
};

//Common functions

function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL) 
        && preg_match('/@.+\./', $email);
}

function isEmailExists($email) {
   $sql = "SELECT * FROM users where email = '".$email."'";
    try {
    $db = getDB();
    $stmt = $db->query($sql);
    $users = $stmt->fetch(PDO::FETCH_OBJ);
    $db = null;
    return $users;
    if(count($users)>0)
    {
        return 'true';
    }
    else
    {
        return 'false';
    }
    } catch(PDOException $e) {
        return 'false';
    }
}

function isEmailPasswordExists($email,$password) {
   $sql = "SELECT * FROM users where email = '".$email."' and password = '".md5($password)."' ";
    try {
    $db = getDB();
    $stmt = $db->query($sql);
    $users = $stmt->fetch(PDO::FETCH_OBJ);
    $db = null;
    return $users;
    if(count($users)>0)
    {
        return 'true';
    }
    else
    {
        return 'false';
    }
    } catch(PDOException $e) {
        return 'false';
    }
}

function updateLastLogin($email) {
   $sql = "UPDATE users SET last_login=".time()." where email = '".$email."'";
    try {
    $db = getDB();
    $stmt = $db->query($sql);
    //$users = $stmt->fetch(PDO::FETCH_OBJ);
    $db = null;
    return 'true';
    
    } catch(PDOException $e) {
        return 'false';
    }
}
function updateUser($data) {
    if(isset($_SESSION['user']['unique'])   ){
   $sql = "UPDATE users SET first_name='".$data['first_name']."',last_name='".$data['last_name']."',gender='".$data['gender']."',address='".$data['address']."',city='".$data['city']."',state='".$data['state']."',postcode='".$data['postcode']."',country='".$data['country']."', updated_on=".time()." where email = '".$_SESSION['user']['email']."'";
    try {
    $db = getDB();
    $stmt = $db->query($sql);
    //$users = $stmt->fetch(PDO::FETCH_OBJ);
    $db = null;
    return 'true';
    
    } catch(PDOException $e) {
        return 'false';
    }
    }
    else
    {
         return 'false';
    }
}

$app->run();