<?php

//Common functions

function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL) 
        && preg_match('/@.+\./', $email);
}

function isEmailExists($email) {
   $sql = "SELECT * FROM users where email = '".$email."'";
    try {
    $db = getDB();
    $stmt = $db->query($sql);
    $users = $stmt->fetch(PDO::FETCH_OBJ);
    $db = null;
    return $users;
    if(count($users)>0)
    {
        return 'true';
    }
    else
    {
        return 'false';
    }
    } catch(PDOException $e) {
        return 'false';
    }
}

function isEmailPasswordExists($email,$password) {
   $sql = "SELECT * FROM users where email = '".$email."' and password = '".md5($password)."' ";
    try {
    $db = getDB();
    $stmt = $db->query($sql);
    $users = $stmt->fetch(PDO::FETCH_OBJ);
    $db = null;
    return $users;
    if(count($users)>0)
    {
        return 'true';
    }
    else
    {
        return 'false';
    }
    } catch(PDOException $e) {
        return 'false';
    }
}

function updateLastLogin($email) {
   $sql = "UPDATE users SET last_login=".time()." where email = '".$email."'";
    try {
    $db = getDB();
    $stmt = $db->query($sql);
    //$users = $stmt->fetch(PDO::FETCH_OBJ);
    $db = null;
    return 'true';
    
    } catch(PDOException $e) {
        return 'false';
    }
}
function updateUser($data) {
    $update_string = " updated_on=".time();
    if(isset($data['first_name']) && $data['first_name']!='')
    {
        $update_string .= " ,first_name='".$data['first_name']."' ";
    }
    if(isset($data['last_name']) && $data['last_name']!='')
    {
        $update_string .= " ,last_name='".$data['last_name']."' ";
    }
     if(isset($data['password']) && $data['password']!='')
    {
        $update_string .= " ,password='".md5($data['password'])."' ";
    }
    if(isset($data['gender']) && $data['gender']!='')
    {
        $update_string .= " ,gender='".$data['gender']."' ";
    }
    if(isset($data['address']) && $data['address']!='')
    {
        $update_string .= " ,address='".$data['address']."' ";
    }
    if(isset($data['city']) && $data['city']!='')
    {
        $update_string .= " ,city='".$data['city']."' ";
    }
    if(isset($data['state']) && $data['state']!='')
    {
        $update_string .= " ,state='".$data['state']."' ";
    }
    if(isset($data['postcode']) && $data['postcode']!='')
    {
        $update_string .= " ,postcode='".$data['postcode']."' ";
    }
    if(isset($data['country']) && $data['country']!='')
    {
        $update_string .= " ,country='".$data['country']."' ";
    }
    
   $sql = "UPDATE users SET ".$update_string." where user_id = '".$data['user_id']."'";
    try {
    $db = getDB();
    $stmt = $db->query($sql);
    //$users = $stmt->fetch(PDO::FETCH_OBJ);
    $db = null;
    return 'true';
    
    } catch(PDOException $e) {
        return 'false';
    }
   
}

function trackUserActivity($data = array()) {
    
    if(isset($data['user_id']) && isset($data['action_type']) )
    {
        $sql = "SELECT * FROM track_user_activity where user_id = ".$data['user_id']." and action_type = '".$data['action_type']."' ";
        try {
        $db = getDB();
        $stmt = $db->query($sql);
        $users = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        
        if(isset($users->action_count))
        {
            $sql = "UPDATE track_user_activity SET action_count = ".($users->action_count + 1 ).", last_action = ".time()."  where trackid = ".$users->trackid."";
            try {
            $db = getDB();
            $stmt = $db->query($sql);
            //$users = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
            } catch(PDOException $e) {
                return false;
            }                                                                       
            return true;
        }
        else
        {
            $sql = "INSERT INTO track_user_activity (action_type, user_id,action_count,  last_action)
                        VALUES ( '".$data['action_type']."', ".$data['user_id'].",1 , ".time()." )";
                try {
                $db = getDB();
                $stmt = $db->query($sql);
                $db = null;
                }catch(PDOException $e) {
                    return false;
                }
            return true;
        }
        } catch(PDOException $e) {
            return false;
        }
    }
    else
    {
        return false;
    }
    
  
}

?>